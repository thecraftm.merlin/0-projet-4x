from tkinter import *

"""def deplacement():
    return (0)"""


class Interface_Bouton:
    def __init__(self, parent:Tk):
        self.frame = Frame(master=parent)
        self.parent = parent
        ## Icones
        self.icones = dict()
        self.icones["haut"] = PhotoImage(file='./icons/icon_haut.png')
        self.icones["bas"] = PhotoImage(file='./icons/icon_bas.png')
        self.icones["gauche"] = PhotoImage(file='./icons/icon_gauche.png')
        self.icones["droite"] = PhotoImage(file='./icons/icon_droite.png')
        self.icones["sante"] = PhotoImage(file='./icons/icon_sante.png')
        self.icones["epee"] = PhotoImage(file='./icons/icon_epee.png')

        ## Widgets
        self.bouton_haut = Button(
            self.frame, image=self.icones["haut"],
            fg="black", bg="white",
            command=lambda:self.parent.event_generate("<<CLICK_HAUT>>"))
        self.bouton_bas = Button(
            self.frame, image=self.icones["bas"],
            fg="black", bg="white",
            command=lambda:self.parent.event_generate("<<CLICK_BAS>>"))
        self.bouton_gauche = Button(
            self.frame, image=self.icones["gauche"],
            fg="black", bg="white",
            command=lambda:self.parent.event_generate("<<CLICK_GAUCHE>>"))
        self.bouton_droite = Button(
            self.frame, image=self.icones["droite"],
            fg="black", bg="white",
            command=lambda:self.parent.event_generate("<<CLICK_DROITE>>"))
        self.btn_soigner = Button(
            self.frame, image=self.icones["sante"],
            fg="black", bg="white",
            command=lambda:self.parent.event_generate("<<CLICK_SOIN>>"))
        self.bouton_epee = Button(
            self.frame, image=self.icones["epee"],
            fg="black", bg="white",
            command=lambda:self.parent.event_generate("<<CLICK_EPEE>>"))
        self.afficher()

    def organiser(self):
        self.btn_soigner.grid(row=0, column=0, sticky=NSEW)
        self.bouton_haut.grid(row=0, column=1, sticky=NSEW)
        self.bouton_bas.grid(row=1, column=1, sticky=NSEW)
        self.bouton_gauche.grid(row=1, column=0, sticky=NSEW)
        self.bouton_droite.grid(row=1, column=2, sticky=NSEW)
        self.bouton_epee.grid(row=0, column=2, sticky=NSEW)

    def afficher(self):
        self.frame.grid(row=0, column=0)

    def refresh(self):
        self.organiser()
        self.afficher()

    def hide(self):
        self.frame.grid_forget()


if __name__ == '__main__':
    fenetre = Tk()
    interf = Interface_Bouton(fenetre)
    interf.organiser()
    fenetre.mainloop()
