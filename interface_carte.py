from tkinter import *
from classe_carte import *
from classe_case import *
import json
#from classe_jeu import Jeu


class Interface_Carte:
    # Initialise tout ce qu'il faut
    def  __init__(self, parent, carte):
        self.images = dict()
        self.charger_images()

        self.largeur = 800
        self.hauteur = 600
        self.carte = carte
        self.fond = Canvas(parent,
                           width = self.largeur, height = self.hauteur,
                           scrollregion=(0,0,100*self.carte.hauteur, 100*self.carte.largeur),
                           bg = 'black')
        self.scroll_x = Scrollbar(parent, orient = HORIZONTAL, command = self.fond.xview)
        self.scroll_y = Scrollbar(parent, orient = VERTICAL, command = self.fond.yview)
        self.fond.config(xscrollcommand = self.scroll_x.set, yscrollcommand = self.scroll_y.set)
        self.dessiner_carte()

    def dessiner_carte(self):
        """TODO : AFFICHER LES CASES AUX BONS ENDROITS"""
        for ligne in range(self.carte.hauteur) :
            for col in range (self.carte.largeur):
                elt = self.carte.get_case(ligne, col).terrain
                self.dessiner_element(ligne, col, elt, "terrain")

    def dessiner_element(self, ligne:int, colonne:int, nom_terrain:str, etiquette:str):
        """TODO : AFFICHER LES UNITES ET LES BATIMENTS AU BON ENDROIT"""
        self.fond.create_image(100*ligne, 100*colonne, anchor=NW,
                               image=self.images[nom_terrain],
                               tag=etiquette)

    def charger_images(self):
        # Images de la carte
        fichier = open(file="./Data/terrains.json", mode="r")
        info_terrains = json.load(fichier)
        fichier.close()
        for nom_terrain in info_terrains:
            nom_image = info_terrains[nom_terrain]["image"]
            self.images[nom_terrain] = PhotoImage(file = "pictures/" + nom_image)

    def deplacer_element(self):
        pass

    #Permet d'afficher la frame
    def afficher(self):
        self.fond.grid(row=0, column=1, rowspan=4)
        self.scroll_x.grid(row = 4, column = 1, sticky = EW)
        self.scroll_y.grid(row = 0, column = 2, rowspan=4, sticky = NS)

    def refresh(self):
        self.afficher()

    def hide(self):
        self.fond.grid_forget()

if __name__ == '__main__':
    c = Carte(10,12)
    u = Unite("Guerrier", 3, 4)
    b = Batiment("HDV", 5, 6)
    c.ajouter_unite(u, 3, 4)
    c.construire_batiment(b, 5, 6)

    fenetre = Tk()
    interf = Interface_Carte(fenetre, c)
    interf.afficher()
    fenetre.mainloop()
