﻿from classe_unite import Unite, NO_UNIT_TEXT
from classe_jeu import Jeu
from tkinter import *
import json
from interface_stats import *


class Interface_Units:

    #Initialisation
    def __init__(self, parent, jeu:Jeu):
        fichier = open(file="./Data/unite.json")
        dico_unite = json.load(fichier)
        fichier.close()

        #Création des images
        self.images = dict()
        for nom_unite in dico_unite:
            filename = dico_unite[nom_unite]["image"]
            self.images[nom_unite] = PhotoImage(file='./pictures/'+filename)
        self.images["noUnit"] = PhotoImage(file='./pictures/unit_nounit.png')
        self.images["flecheG"] = PhotoImage(file='./icons/icon_left.png')
        self.images["flecheD"] = PhotoImage(file='./icons/icon_right.png')

        self.jeu = jeu
        self.parent = parent
        self.unite_affichee = jeu.unite_courante()

        #Création des widgets
        self.frame = Frame(master=parent)
        self.stats = Label(
            master=self.frame,
            anchor=W,
            justify=LEFT,
            text="")
        self.btn_flecheG = Button(
            master=self.frame,# height=50, width=128,
            image=self.images["flecheG"],
            command=self.on_btn_gauche_click)
        self.btn_flecheD = Button(
            master=self.frame, # height=50, width=128,
            image=self.images["flecheD"],
            command=self.on_btn_droite_click)
        self.image_unit = Label(master=self.frame, image=None)

        #Affichage des widgets et configuration
        self.actualiser()
        self.refresh()


    def organiser(self):
        self.btn_flecheG.grid(row=0, column=0)
        self.image_unit.grid(row=0, column=1)
        self.btn_flecheD.grid(row=0, column=2)
        self.stats.grid(row=1, column=0, columnspan=3, sticky=EW)

    def afficher(self):
        self.frame.grid(row=1, column= 3, sticky = "n")

    def hide(self):
        self.frame.grid_forget()

    def refresh(self):
        self.organiser()
        self.afficher()

    def actualiser(self):
        if self.jeu.nb_unites()==0:
            print(self.images["noUnit"], "veuillez sélectioner une unité")
        else:
            self.stats.config(text = str(self.unite_affichee))
            self.image_unit.config(image = self.images[self.unite_affichee.nom])

    ### Callbacks des boutons ###

    def on_btn_gauche_click(self):
        self.unite_affichee = self.images[nom_unite-1]
        self.actualiser()

    def on_btn_droite_click(self):
        self.unite_affichee = self.images[nom_unite+1]
        self.actualiser()

#ces deux fonctions ne marchent pas et je ne sais pas pourquoi

if __name__ == '__main__':
    # TODO : tester l'interface avec une liste vide
    liste_unites = []
    liste_unites = [Unite("Guerrier", 8, 3), Unite("Archer", 7, 5), Unite("Chevalier", 8, 6)]
    game = Jeu(carte="une carte en attendant mieux",
               batiments=None,
               unites=liste_unites)

    fenetre = Tk()
    interface_units = Interface_Units(fenetre, game)
    fenetre.mainloop()
