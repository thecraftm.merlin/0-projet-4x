Projet 4X
==

Idée principale du jeu
=

Le jeu tirera ses principes de clash of clans et de polytopia.
Le principe sera de détruire les camps adverses accessibles niveaux par 
niveaux pour pouvoir débloquer les suivants, 
gagner des ressources développer son propre camps.

Les attaques face aux adversaires seront simple, détruire leurs quartiers
qui seront défendu par des bâtiments défensifs. Après une attaque réussie
la contre attaque des ennemis se fera dans le village du joueur qui devra 
le défendre grâce à des unités qu'il déploieras et des bâtiments défensifs 
qu'il aura établis au préalable.

Chaque attaque sera de plus en plus compliqué il faudra donc améliorer ses
unités et ses défenses. Les attaques seront également plus chère car pour 
attaquer un village il faudras payer une somme en or.
Si l'attaque est un succès alors elle débloque le niveau suivant même 
si la défense est un échec. 
Cependant si la défense échoue alors le joueur perdras
les ressources prises à l'adversaire lors de l'attaque plus un pourcentage 
aléatoire de ses ressources qu'il possédait déja.


Aspect technique
=

**Les ressources et leurs fonctions**
- L'or : Il servira à effectuer des attaques et améliorer ses unités principalement
- Le métal : Seras utile pour la fabrication de bâtiments défensifs 
- Le bois : Serviras la construction des bâtiments et surtout leur réparation



**Les batiments et leurs fonctions**
- L'hôtel de ville : permettras de créer les unités, seras le coeur de la ville
- La scierie : Permettras le stockage du bois
- La banque : Permettras le stockage de l'or
- La forge : Permettras le stockage du fer
- Canon / Catapulte / ... : Bâtiments défensif classique qui défendras le village
- Les barricades : Feront rempart aux unités, devront être détruite pour passer

**Fonctionnements des ennemis**
- L'attaque des ennemis s'effectue lorsque le joueur à réussis à piller leurs villages, ils vont 
donc attaquer le village du joueur avec comme seul objectif l'HDV, s'il est détruit, c'est game over.
Si des obstacles se trouvent sur leur passage ils les détruiront (tout les autres batiments au final)

**Autres:**
- Si l'hotel de ville est détruit lors d'une attaque des ennemis alors 50% de la population mourras
- Si les batiments de stockage sont détruits alors ils ne contiendront plus que 25% de leurs ressources 
initiales.

Schéma
==

![IMG_4609](/uploads/a8b7b40d417ed1cce5677f03c4601281/IMG_4609.jpg)

**Aide**
http://vincent_jarc.gitlab.io/informatique/