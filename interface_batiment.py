from classe_batiment import *
from classe_jeu import *
from classe_carte import *
from classe_unite import *

class Interface_batiment:
    def __init__(self, parent, jeu:Jeu):
        self.jeu = jeu
        self.frame = Frame(master=parent, width = 100, height=75)
        ...

    def organiser(self):
        ...



if __name__ == "__main__":
    carte = Carte(filename="map01.json")
    jeu = Jeu(carte=carte)
    jeu.ressource.ajouter_Or(1000)
    jeu.construire("Scierie",3,2)

    fen = Tk()
    inter = Interface_batiment(fen, jeu)
    inter.frame.grid()
    fen.mainloop()