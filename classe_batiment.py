import json


class Batiment:
    # Initialise tout
    def __init__(self, nom:str, ligne:int, colonne:int):
        fichier = open(file="./Data/batiment.json", mode="r")
        info = json.load(fichier)
        fichier.close()
        self.nom = nom
        self.pv = info[nom]["pv"]
        self.pv_max = self.pv
        self.cout = info[nom]["cout"]
        self.production_or = info[nom]["or"]
        self.production_bois = info[nom]["bois"]
        self.production_metal = info[nom]["metal"]
        self.attaque = info[nom]["degat"]
        self.distance = info[nom]["distance"]
        self.niveau = 1
        self.pillage = False
        self.construction = False
        self.ligne, self.colonne = ligne, colonne

    def set_coordonnees(self, ligne, colonne):
        self.ligne, self.colonne = ligne, colonne


    # Augmentation du stockage des ressources
    def augmenter_production_bois(self, montant:int):
        self.production_bois += montant

    def augmenter_production_or(self, montant):
        self.production_or += montant

    def augmenter_production_metal(self, montant):
        self.production_metal += montant


    def augmenter_niveau(self):
        """Augmente niveau du batiment"""
        self.niveau += 1


    def retire_pv(self, valeur):
        """Diminue les pv du bâtiment"""
        self.pv -= valeur
        if self.pv < 0:
            self.pv = 0

    def ajout_pv(self, valeur):
        """Augmente les PV du bÃƒÂ¢timent"""
        self.pv += valeur
        if self.pv > self.pv_max:
            self.pv = self.pv_max

    def piller(self):
        """Place le bâtiment à l'état pillé"""
        self.pillage = True

    def reparer(self):
        """Ajoute 50PV au batiment et retire l'état pillé s'il est réparé"""
        self.regain_pv(50)
        if self.pv == self.pv_max:
            self.pillage = False

    # Production des ressources
    def recolter_or(self):
        if self.pillage == True:
            return 0
        else:
            return self.production_or

    def recolter_bois(self):
        if self.pillage == True:
            return 0
        else:
            return self.production_bois

    def recolter_metal(self):
        if self.pillage == True:
            return 0
        else:
            return self.production_metal

    #Attaque des batiments

    def calculer_distance(self, other):
        ...

    def peut_bombarder(self, other)->bool:
        ...

    def attaquer(self, other):
        # TODO : tenir compte de la portée du bâtiment
        degats = self.attaque
        other.retire_pv(degats)

    #Pouvoir poser bÃ¢timent
    def poser_batiment(self):
        # WARNING : est-ce vraiment utiles ?
        self.construction = True


    def __str__(self):
        txt = self.nom
        txt += "\t(niv " + str(self.niveau) + ")\n"
        txt += "Pv : " + str(self.pv) + "/" + str(self.pv_max) + "\n"
        txt += "Production Bois : " + str(self.production_bois) + "\n"
        txt += "Production Métal : " + str(self.production_metal) + "\n"
        txt += "Production Or : " + str(self.production_or)
        return txt


if __name__ == '__main__':
    Scierie = Batiment("Scierie")
    print(Scierie)