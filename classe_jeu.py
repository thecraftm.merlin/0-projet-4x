from classe_batiment import *
from classe_case import *
from classe_unite import *
from random import *
from tkinter import *
from classe_carte import Carte
from classe_ressource import Ressource
import json


class Jeu:

    def __init__(self, carte: Carte, batiments: list = None, unites: list = None):
        self.ressource = Ressource()
        self.mappe = carte
        self.stage_accessible = 0
        self.population = 0
        if batiments is None:
            self.batiment = []
        else:
            self.batiment = batiments

        if unites is None:
            self.unite = []
        else:
            self.unite = unites

        self.batiment_barbare = []  # On devrait ajouter le HdV au départ non ?
        self.unite_barbare = []

        self.batiment_selectionne = 0  # ou self.batiment[0] ???
        self.unite_selectionnee = 0  # ou self.unite[0] ???
        self.unite_barbare_selectionnee = 0  # ou self.unite_barbare[0] ???


    def __str__(self):
        txt_ressource = "Les ressources du village sont : " \
                        + str(self.ressource.gold()) + " Or / " \
                        + str(self.ressource.metal()) + " Metal / " \
                        + str(self.ressource.bois()) + " Bois"
        txt_niv = "Les niveaux sont accessibles jusqu'au n°" + str(self.stage_accessible)
        res = txt_ressource + "\n" + txt_niv
        return res

    def __repr__(self):
        return str(self)


    ###   RESSOURCES   ###

    def recolter_ressources(self):
        for bat in self.batiment:
            self.ressource.ajouter_Or(bat.recolter_or())
            self.ressource.ajouter_Bois(bat.recolter_bois())
            self.ressource.ajouter_Metal(bat.recolter_metal())

    ### GESTION DES BATIMENTS ###
    def nb_batiments(self):
        return len(self.batiment)

    def construire(self, nom_bat, ligne, colonne):
        bat = Batiment(nom_bat, ligne, colonne)
        case = self.mappe.get_case(ligne, colonne)
        if nom_bat != "HDV" and self.ressource.gold() >= bat.cout and case.constructible:
            self.batiment.append(bat)
            self.mappe.mappe[ligne][colonne].construire_batiment(bat)
            self.ressource.retirer_Or(bat.cout)

    def detruire(self, ligne, colonne):
        self.mappe.mappe[ligne][colonne].detruire_batiment()
        #TODO :
        # - rechercher l'indice du batiment dans self.batiment
        # - le sortir de la liste self.batiment

    def get_pos_hdv_joueur(self):
        return self.batiment[0].ligne, self.batiment[0].colonne
    def batiment_courant(self):
        # TODO
        ...

    def batiment_suivant(self):
        # TODO
        ...

    def batiment_precedent(self):
        # TODO
        ...

    def get_batiment(self, ligne, colonne):
        # TODO
        ...

    ### GESTION DES UNITES ###

    def nb_unites(self):
        return len(self.unite)

    def unite_courante(self):
        if self.nb_unites() == 0:
            return None
        else:
            return self.unite[self.unite_selectionnee]

    def unite_suivante(self):
        n = self.nb_unites()
        if n > 0:
            self.unite_selectionnee = (self.unite_selectionnee + 1) % n
        return self.unite_courante()

    def unite_precedente(self):
        n = self.nb_unites()
        if n > 0:
            self.unite_selectionnee = (self.unite_selectionnee - 1) % n
        return self.unite_courante()

    def get_unite(self, ligne, colonne):
        """
        TODO :
        - recherche s'il y a une unité à la case indiquée (voir classe case et méthode get_unite)
        - s'il y en a une, rechercher sa position dans self.unites
        - modifier self.unite_selectionnee
        """
        ...

    def entrainer_unite(self, nom_unit, ligne, colonne):
        """ TODO :
        - Reprendre le code des bâtiments pour créer une nouvelle unité
        """
        pass

    def deplacer_unite(self, direction):
        """ TODO :
        - Utiliser self.unite, self.unite_selectionnee ou self.unite_courante()
        - ainsi que les méthodes des Unit pour le déplacemcent
        - mais aussi les méthodes des cases ou carte pour savoir si la case est accessible
        """
        pass

    def deplacement_IA(self, objectif):
        # on prend l'unité barbare sélectionnée
        # on regarder l'objectif (le HdV) et les voisins
        # on préfère tataner le joueur s'il est à côté
        # sinon on se déplace vers le nord, l'est, l'ouest ou l'est avec la fonction deplacer
        case_actuelle = self.mappe.get_case(pos_ennemis, )

        if case_actuelle.contient_batiment() :
            attaque_ennemis = randint(0, batiment.pv_max)
            batiment.pv -= attaque_ennemis

        elif case_actuelle.contient_unite():
            pass
        else:
            if pos_ennemis[x] > objectif[x]:
                deplacement_ennemis[x - 1][y]  # Nécessite la création d'une fonction de déplacement
            elif pos_ennemis[x] < objectif[x]:
                deplacement_ennemis[x + 1][y]
            elif pos_ennemis[y] > objectif[y]:
                deplacement_ennemis[x][y - 1]
            elif pos_ennemis[y] < objectif[y]:
                deplacement_ennemis[x][y + 1]


    ### AUTRE ###
    def tour_IA(self):
        """A SIMPLIFIER"""
        objectif = get_pos_hdv()  # de type list contient les coordonnées de l'hdv, objectif principal des ennemis
        for all_ennemis in self.unite_barbare:  # effectuera l'action pour tous les ennemis
            deplacement_IA(objectif)

    def augmenter_niveau(self):
        self.stage_accessible += 1

    def fin_tour(self):
        # TODO :
        #  - modifier le nombre de tours !!!
        #  - lancer le tour de l'IA
        self.recolter_ressources()
        if self.a_perdu():
            pass


    def a_perdu(self) -> bool:
        """pour savoir si le joueur a perdu
        CECI EST UN EXEMPLE :
            - il faudra créer la méthode est_detruit() pour les Batiment
            - je suppose ici que self.batiment[0] est forcément l'hôtel de ville
        """
        if self.nb_batiments() == 0 or self.batiment[0].est_detruit():
            return True
        else:
            return False

    def a_gagne(self) -> bool:
        """pour savoir si le joueur a gagné
        CECI EST AUSSI UN EXEMPLE"""
        if len(self.batiment_barbare) == 0:
            return True
        else:
            return False


if __name__ == '__main__':
    carte = Carte(filename="map01.json")
    game = Jeu(carte=carte)
    print(game.ressource)
    game.ressource.ajouter_Or(1000)
    print(game.ressource)

    game.construire("Scierie", 3,2)
    print(game.mappe.mappe[3][2])
    print(game.ressource)

    game.entrainer_unite("Guerrier", 4,5)
    print(game.mappe.mappe[4][5])
    print(game.ressource)
