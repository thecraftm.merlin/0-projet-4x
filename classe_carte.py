from classe_case import *
from random import choice
from classe_case import *
import json



class Carte():
    def __init__(self, largeur:int=-1, hauteur:int=-1, filename:str=""):
        """
        Permet de créer une carte aléatoirement ou à partir d'un fichier.
        WARNING :
        - Soit filename fait référence à un nom de fichier et dans ce cas, largeur et
        hauteur ne doivent pas être renseignés
        - Soit filename n'est pas renseigné et dans ce cas il faut indiquer la largeur
        et la hauteur désirée pour la carte
        """
        if hauteur > 0 and largeur > 0:
            self.hauteur = hauteur
            self.largeur = largeur
            self.creer_map_aleatoire()
        else:
            self.charger_map(filename)

    def creer_map_vide(self):
        """Crée une carte remplie d'herbe"""
        self.nom = "Carte vierge"
        self.mappe = []
        for i in range(self.hauteur):
            ligne = []
            for j in range(self.largeur):
                ligne.append(Case(i, j, "Prairie"))
            self.mappe.append(ligne)

    def creer_map_aleatoire(self):
        """Crée une carte aléatoire"""
        fichier = open(file="Data/terrains.json", mode= "r")
        list_terrains = list(json.load(fichier).keys())
        fichier.close()

        self.nom = "Carte aléatoire"
        self.mappe = []
        for i in range(self.hauteur):
            ligne = []
            for j in range(self.largeur):
                terrain = choice(list_terrains)
                ligne.append(Case(i, j, terrain))
            self.mappe.append(ligne)

    def charger_map(self, filename):
        fichier = open(file="Maps/"+filename, mode= "r")
        datas = json.load(fichier)
        fichier.close()

        list_terrains = datas["terrains"]
        self.nom = datas["name"]
        self.mappe = []
        self.hauteur = len(list_terrains)
        self.largeur = len(list_terrains[0])
        for i in range(self.hauteur):
            ligne = []
            for j in range(self.largeur):
                ligne.append(Case(i, j, list_terrains[i][j]))
            self.mappe.append(ligne)

    def afficher(self):
        """affiche la carte"""
        print("\t" + self.nom + ":")
        for ligne in carte.mappe:
            for case in ligne:
                print(case.terrain, end=", ")
            print()

    def modifier(self, coord, case:Case):
        """ Remplace la valeur d'une coordonée par une autre valeur aux mêmes coordonnées"""
        ligne, colone = coord
        self.mappe[ligne][colone] = case

    def est_valide(self, coord:tuple)->bool:
        """ Renvoie False si coord est en dehors de la carte et True sinon"""
        ligne, colone = coord
        if 0 <= ligne < self.hauteur and 0 <= colone < self.largeur:
            return True
        else:
            return False

    def a_gauche(self, coord:tuple)->tuple:
        ligne, colone = coord
        return ligne, colone - 1

    def a_droite(self, coord:tuple)->tuple:
        ligne, colone = coord
        return ligne , colone+1

    def en_haut(self, coord:tuple)->tuple:
        ligne, colone = coord
        return ligne-1, colone

    def en_bas(self, coord:tuple)->tuple:
        ligne, colone = coord
        return ligne+1, colone

    def sont_a_cote(self, coord1:tuple, coord2:tuple)->bool:
        if self.a_gauche(coord1) == coord2:
            return True
        elif self.a_droite(coord1) == coord2:
            return True
        elif self.en_haut(coord1) == coord2:
            return True
        elif self.en_bas(coord1) == coord2:
            return True
        else:
            return False

    ### REUTILISATION DES METHODES DES CASES ###

    ##### CACHE / DECOUVERT #####

    def decouvrir(self, ligne:int, colonne:int):
        """ A compléter à l'aide de classe_case.py """
        ...

    def est_decouverte(self, ligne:int, colonne:int):
        """ A compléter à l'aide de classe_case.py """
        ...

    ##### BATIMENTS #####

    def contient_batiment(self, ligne:int, colonne:int):
        """ A compléter à l'aide de classe_case.py """
        ...

    def construire_batiment(self, bat, ligne:int, colonne:int):
        """ A compléter à l'aide de classe_case.py """
        ...

    def detruire_batiment(self, ligne:int, colonne:int):
        """ A compléter à l'aide de classe_case.py """
        ...

    ##### UNITES #####

    def contient_unite(self, ligne:int, colonne:int):
        """ A compléter à l'aide de classe_case.py """
        ...

    def ajouter_unite(self, new_unit:Unite, ligne:int, colonne:int):
        """ A compléter à l'aide de classe_case.py """
        ...

    def enlever_unite(self, ligne:int, colonne:int):
        """ A compléter à l'aide de classe_case.py """
        ...


    ### A REVOIR ###

    def get_case(self, ligne:int , col:int)->Case:
        """ Renvoie la case indiquée par les paramètres ligne et colonne """
        return self.mappe[ligne][col]

    def get_unite(self, ligne:int , col:int)->Unite:
        """
        Renvoie l'unité présente sur la case indiquée par les
        paramètres ligne et colonne
        """
        return ...

    def get_batiment(self, ligne:int , col:int)->Batiment:
        """
        Renvoie le bâtiment présent sur la case indiquée par les
        paramètres ligne et colonne
        """
        return ...

    def get_terrain(self, ligne:int , col:int)->str:
        """
        Renvoie le terrain présent sur la case indiquée par les
        paramètres ligne et colonne
        """
        return ...

    def get_emplacement(self, bat:Batiment)->tuple:
        """ Donne l'emplacement d'un batiment et (-1, -1) s'il n'est pas présent"""
        # TODO : parcourir self.mappe à la main
        for ligne in ...:
            for colonne in ...:
                ...
        res = self.mappe.index(bat)      #renvoie une erreur si le bat rechercher n'est pas present
        return res



if __name__=='__main__':
    carte = Carte(largeur=8, hauteur=6)
    carte.afficher()
    print("\t-------------")
    carte = Carte(filename="map01.json")
    carte.afficher()
    print("\t-------------")
    print("AJOUTER QUELQUES TESTS SUPPLEMENTAIRES")