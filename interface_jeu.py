from tkinter import *
from interface_bouton import *
from interface_carte import *
from interface_unit import *
from interface_menu import *
from interface_stats import *
from classe_jeu import Jeu

class Interface_Jeu:
    # Initialise tout ce qu'il faut
    def  __init__(self, parent, jeu):
        self.parent = parent
        self.jeu = jeu
        self.frame_menu = Interface_Menu(self.parent)
        self.generer()
        self.connecter()

    def generer(self):
        self.frame_bouton = Interface_Bouton(self.parent)
        self.frame_carte = Interface_Carte(self.parent, self.jeu.mappe)
        self.frame_stats = Interface_Stats(self.parent, self.jeu)
        self.frame_units = Interface_Units(self.parent, self.jeu)
        self.frame_menu.btn_nvl_partie.config(command = self.refresh)

    def connecter(self):
        self.parent.bind("<<CLICK_SOIN>>", self.on_btn_soigner_click)
        self.parent.bind("<<CLICK_EPEE>>", self.on_btn_attaquer_click)
        self.parent.bind("<<CLICK_HAUT>>", self.on_btn_haut_click)
        self.parent.bind("<<CLICK_BAS>>", self.on_btn_haut_click)
        self.parent.bind("<<CLICK_GAUCHE>>", self.on_btn_haut_click)
        self.parent.bind("<<CLICK_DROITE>>", self.on_btn_haut_click)

    def hide(self):
        self.frame_menu.changer_menu2()
        self.frame_bouton.hide()
        self.frame_carte.hide()
        self.frame_units.hide()
        self.frame_stats.hide()
        self.frame_menu.btn_nvl_partie.config(command=self.refresh)

    def refresh(self):
        self.frame_bouton.refresh()
        self.frame_carte.refresh()
        self.frame_units.refresh()
        self.frame_stats.refresh()
        self.frame_menu.refresh()
        self.frame_menu.btn_config.config(command=self.hide)

    def on_btn_soigner_click(self, event):
        # TODO : soigner l'unité courante et actualiser ses stats
        ...
        print("Soigner")

    def on_btn_haut_click(self, event:Event):
        # TODO : déplacer l'unité courante sur la carte, actualiser l'interface
        ...
        print("aller en haut")

    def on_btn_attaquer_click(self, event):
        # TODO : à voir si c'est utile
        ...
        print("A l'attaaaaaque !")



if __name__ == '__main__':
    carte = Carte(12, 8)
    jeu = Jeu(carte)
    fenetre = Tk()
    interf = Interface_Jeu(fenetre, jeu)
    interf.refresh()
    fenetre.mainloop()
