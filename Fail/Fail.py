def actualiser_ressource(self):
    fichier = open(file="./Data/batiment_ingame.json", mode="r")
    donnees_bat = json.load(fichier)
    fichier.close()
    self.dico['Or'] = donnees_bat["Banque"]["or"]
    self.dico['Metal'] = donnees_bat["Forge"]["metal"]
    self.dico['Bois'] = donnees_bat["Scierie"]["bois"]


def push_ressource(self):
    fichier_read = open(file="./Data/batiment_ingame.json", mode="r")
    donnees_bat = json.load(fichier_read)
    fichier_read.close()
    donnees_bat["Banque"]["or"] = self.dico['Or']
    donnees_bat["Forge"]["metal"] = self.dico['Metal']
    donnees_bat["Scierie"]["bois"] = self.dico['Bois']
    fichier_write = open(file="./Data/batiment_ingame.json", mode="w")
    json.dump(donnees_bat, fichier_write)
    fichier_write.close()