from classe_jeu import *
from interface_jeu import *
from classe_carte import *
from classe_unite import Unite

carte = Carte(8, 10)
liste_unites = [Unite("Guerrier", 1,2), Unite("Archer", 2,2)]
jeu = Jeu(carte, unites=liste_unites)

fenetre = Tk()
Interface = Interface_Jeu(fenetre,jeu)
Interface.refresh()
fenetre.mainloop()
