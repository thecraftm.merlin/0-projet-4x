# Commentaires

## 2022-12-02

- Le projet avance bien : les interfaces ont avancé
- Il reste des points à développer côté backend
- Les images nécessaires au jeu ne sont pas toutes en lignes
- La gestion des ressources doit être éclaircie : qui produit du bois ? Les cases de forêt ou les scieries ? Qui stocke ce bois ? Les bâtiments ou le joueur ?

## 2022-10-05

- 8 tickets ouverts, 6 ouverts et 1 fermé
- travail réparti à peu près équitablement sur 4 devOps, 1 n'a rien posté depuis le début
- 4 fichiers déposés
- Des remarques faites dans les tickets correspondants : 
    - [x] Batiments
    - [ ] Unités
    - [ ] Jeu
    - [ ] Case
    - [ ] Map

