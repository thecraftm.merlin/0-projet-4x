from tkinter import *
from classe_jeu import Jeu

class Interface_Stats:
    def  __init__(self, parent, jeu:Jeu):
        self.jeu = jeu
        self.default_txt = "NO INFORMATION AVAILABLE"
        self.game_stats_frame = Frame(master = parent)
        self.qte_ressource = Label(master=self.game_stats_frame, text=self.default_txt)
        self.level_access = Label(master=self.game_stats_frame, text=self.default_txt)
        self.qte_population = Label(master=self.game_stats_frame, text=self.default_txt)

    def organiser(self):
        self.qte_ressource.grid(row=0, column=0)
        self.level_access.grid(row=1, column=0)
        self.qte_population.grid(row=2, column=0)

    def afficher(self):
        self.game_stats_frame.grid(row=3, column=3)

    def get_ressource(self):
        txt_ressource = "Les ressources du village sont : " \
                        + str(self.jeu.ressource.gold()) + " Or / " \
                        + str(self.jeu.ressource.metal()) + " Metal / " \
                        + str(self.jeu.ressource.bois()) + " Bois"
        res = txt_ressource
        return res

    def get_level(self):
        res = "Les niveaux sont accessibles jusqu'au n°" + str(self.jeu.stage_accessible)
        return res

    def get_population(self):
        res = "Nombre d'habitants : " + str(jeu.population)
        return res

    def refresh(self):
        self.qte_ressource.config(text= self.get_ressource())
        self.level_access.config(text= self.get_level())
        self.qte_population.config(text= self.get_population())
        self.organiser()
        self.afficher()

    def hide(self):
        self.game_stats_frame.grid_forget()


if __name__ == '__main__':
    def augmenter_nv(j:Jeu, i:Interface_Stats):
        j.augmenter_niveau()
        i.refresh()

    def augmenter_bois(j:Jeu, i:Interface_Stats):
        j.ressource.ajouter_Bois(10)
        i.refresh()

    jeu = Jeu([])

    fenetre = Tk()

    stats = Interface_Stats(fenetre, jeu)
    stats.refresh()


    add_button = Button(master=fenetre,
                        text="Ajouter bois",
                        command=lambda:augmenter_bois(jeu, stats))
    add_button.grid(row=1, column=1)

    lvl_button = Button(master=fenetre,
                        text="Ajouter niveau",
                        command=lambda:augmenter_nv(jeu, stats))
    lvl_button.grid(row=1, column=2)

    fenetre.mainloop()
