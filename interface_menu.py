from tkinter import *
from interface_jeu import *

class Interface_Menu:
    def __init__(self, parent):
        self.parent = parent
        self.creer_menu1()

    def creer_menu1(self):
        self.menu_frame = Frame(master=self.parent)
        self.btn_charger = Button(master=self.menu_frame,
                                  text='Charger',
                                  command=None)
        self.btn_sauv = Button(master=self.menu_frame,
                               text='Sauvegarder',
                               command=None)
        self.btn_reprendre = Button(master=self.menu_frame,
                                    text='Reprendre',
                                    command=None)
        self.btn_nvl_partie = Button(master=self.menu_frame,
                                     text='Nouvelle partie',
                                     command=None)
        self.btn_fermer = Button(master=self.menu_frame,
                                 text='Quitter',
                                 command=self.fermer_fenetre)

        self.config_menu1()
        self.organiser_menu1()
        self.afficher_menu1()

    def config_menu1(self):
        self.menu_frame.configure(bg='#ffffff')
        self.btn_charger.config(height=5, width=15,
                                bg="#3bc80a", activebackground="#3bc80a",
                                fg="white", relief="flat", borderwidth=0)
        self.btn_sauv.config(height=5, width=15,
                             bg="#3bc80a", activebackground="#3bc80a",
                             fg="white", relief="flat", borderwidth=0)
        self.btn_reprendre.config(height=5, width=15,
                                  bg="#3bc80a", activebackground="#3bc80a",
                                  fg="white", relief="flat", borderwidth=0)
        self.btn_nvl_partie.config(height=5, width=15,
                                   bg="#3bc80a",activebackground="#3bc80a",
                                   fg="white", relief="flat",borderwidth=0)
        self.btn_fermer.config(height=5, width=15,
                               bg="#3bc80a",activebackground="#3bc80a",
                               fg="white", relief="flat",borderwidth=0)

    def organiser_menu1(self):
        self.btn_charger.grid(row=0, column=0, padx=215, pady=5)
        self.btn_sauv.grid(row=1, column=0, padx=215, pady=5)
        self.btn_reprendre.grid(row=2, column=0, padx=215, pady=5)
        self.btn_nvl_partie.grid(row=3, column=0, padx=215, pady=5)
        self.btn_fermer.grid(row=4, column=0, padx=215, pady=5)

    def afficher_menu1(self):
        self.menu_frame.grid(row=0, column=0)

    def effacer_menu1(self):
        self.menu_frame.destroy()

    def creer_menu2(self):
        self.frame_menu2   = Frame(master=self.parent)
        self.img_parametre = PhotoImage(file='./icons/logo_parametre.png')
        self.btn_config    = Button(master=self.frame_menu2,
                                    image=self.img_parametre,
                                    command=self.changer_menu2)
        self.config_menu2()
        self.organiser_menu2()
        self.afficher_menu2()
    def config_menu2(self):
        self.btn_config.config(height=75, width=75,
                               relief="flat", borderwidth=0)
        self.frame_menu2.configure(bg='grey')

    def afficher_menu2(self):
        self.frame_menu2.grid(row=0, column=3, sticky="ne")

    def organiser_menu2(self):
        self.btn_config.grid(row=0, column=0)

    def effacer_menu2(self):
        self.frame_menu2.destroy()

    def changer_menu(self):#On pourra améliorer cette fonction au fil du temps
        self.effacer_menu1()
        self.creer_menu2()
        self.afficher_menu2()

    def changer_menu2(self):  # On pourra améliorer cette fonction au fil du temps
        self.effacer_menu2()
        self.creer_menu1()
        self.afficher_menu1()

    def fermer_fenetre(self):
        self.parent.destroy()

    def refresh(self):
        self.changer_menu()



if __name__ == '__main__':
    fenetre = Tk()
    fenetre.title("MENU")
    fenetre.geometry("600x500")

    menu = Interface_Menu(fenetre)

    fenetre.mainloop()
