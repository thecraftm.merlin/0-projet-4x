﻿import json

NO_UNIT_TEXT = "\tPas d'unité sélectionnée\nNOM :\nPV :\nSHIELD :\nATTACK :\nMOUV :\nRANGE :\nBOMBARDER :"

class Unite():

    def __init__(self, nom: str, ligne : int, colonne : int):
        fichier = open(file="./Data/unite.json")
        dico_unite = json.load(fichier)
        fichier.close()

        self.surnom = dico_unite[nom]['surnom']
        self.nom = nom
        self.pv = dico_unite[nom]['pv']
        self.pv_max = dico_unite[nom]['pv']
        self.shield = dico_unite[nom]['shield']
        self.shield_max = dico_unite[nom]['shield']
        self.deplacement = dico_unite[nom]['deplacement']
        self.deplacement_max = dico_unite[nom]['deplacement']
        self.attack = dico_unite[nom]['attack']
        self.bombard = dico_unite[nom]['bombarder']
        self.niv = 1
        self.range = dico_unite[nom]['range']
        self.ligne = ligne
        self.colonne = colonne


    def retire_pv(self, valeur):
        self.pv -= valeur
        if self.pv - valeur < 0:
            self.pv = 0


    def retire_shield(self, valeur):
        if valeur > self.shield:
            reste = valeur - self.shield
            self.shield = 0
        else:
            reste = 0
            self.shield = self.shield - valeur
        return reste


    def ajout_pv(self, valeur):
        if self.pv + valeur > self.pv_max:
            self.pv = self.pv_max
        else:
            self.pv += valeur

    def ajout_shield(self, valeur):
        if self.shield + valeur > self.shield_max:
            self.shield = self.shield_max
        else:
            self.shield += valeur

    def recuperer_deplacements(self):
        self.deplacement = self.deplacement_max

    def __str__(self):
        txt = "\t" + self.nom + ":\n"
        txt += "NOM :" + str(self.surnom) + "\n"
        txt += "PV :" + str(self.pv) + "/" + str(self.pv_max) + "\n"
        txt += "SHIELD :" + str(self.shield) + "/" + str(self.shield_max) + "\n"
        txt += "ATTACK :" + str(self.attack) + "\n"
        txt += "MOUV :" + str(self.deplacement)  +"/"+str(self.deplacement_max)+ "\n"
        txt += "RANGE :" + str(self.range)  + "\n"
        txt += "BOMBARDER :" + str(self.bombard) + "\n"
        return txt


    def soigner(self):
        qte = int(0.5*self.pv_max)
        self.ajout_pv(qte)
        qte2 = int(0.5*self.shield_max)
        self.ajout_shield(qte2)
        self.deplacement = 0


    def attaquer(self, other):
        degats = self.attack
        degats_restants = other.retire_shield(degats)
        other.retire_pv(degats_restants)
        self.deplacement = 0


    def bombarder(self, other):
        bombe = self.bombard
        other.retire_pv(bombe)
        self.deplacement = 0


    def est_mort(self) -> bool:
        if self.pv <= 0:
            return True
        else:
            return False


    def deplacement_haut(self):
        self.ligne -= 1
        self.deplacement -= 1


    def deplacement_bas(self):
        self.ligne += 1
        self.deplacement -= 1


    def deplacement_gauche(self):
        self.colonne -= 1
        self.deplacement -= 1


    def deplacement_droite(self):
        self.colonne += 1
        self.deplacement -= 1


    def get_coordonnées(self):
        return self.ligne, self.colonne


if __name__ == '__main__':
    guerrier1 = Unite("Guerrier", 0, 0)
    guerrier2 = Unite("Archer", 0, 1)
    print(guerrier1)
    print(guerrier2)
    guerrier1.attaquer(guerrier2)
    guerrier2.attaquer(guerrier1)
    print(guerrier1)
    print(guerrier2)
