import json


class Ressource:
    def __init__(self, gold=0, bois=0, metal=0):
        self.dico = {'Or': gold, 'Bois': bois, 'Metal': metal}

    def __str__(self):
        return "Or : {0}, Bois : {1}, Metal : {2}".format(self.gold(),self.bois(), self.metal())
    def gold(self):
        return self.dico['Or']
    def bois(self):
        return self.dico['Bois']
    def metal(self):
        return self.dico['Metal']
    def ajouter_Bois(self, qte: int):
        self.dico['Bois'] += qte

    def ajouter_Or(self, qte: int):
        self.dico['Or'] += qte

    def ajouter_Metal(self, qte: int):
        self.dico['Metal'] += qte

    def retirer_Bois(self, qte: int):
        if self.dico['Bois'] - qte < 0:
            self.dico['Bois'] = 0
        else:
            self.dico['Bois'] -= qte

    def retirer_Or(self, qte: int):
        if self.dico['Or'] - qte < 0:
            self.dico['Or'] = 0
        else:
            self.dico['Or'] -= qte
    def retirer_Metal(self, qte: int):
        if self.dico['Metal'] - qte < 0:
            self.dico['Metal'] = 0
        else:
            self.dico['Metal'] -= qte
