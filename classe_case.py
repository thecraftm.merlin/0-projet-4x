from classe_unite import Unite
from classe_batiment import Batiment
import json

class Case():
    def __init__(self, ligne, col, nom_terrain:str):
        fichier = open(file="./Data/terrains.json")
        dico_terrain = json.load(fichier)
        fichier.close()

        self.terrain = nom_terrain
        self.constructible = dico_terrain[self.terrain]["constructible"]
        self.bat   = None
        self.unite = None
        self.coord = (ligne, col)
        self.cache = True

    ##### COORDONNEES #####

    def set_coordonnees(self, ligne, col):
        self.coord = (ligne, col)

    def get_coordonnees(self):
        return self.coord

    def ligne(self):
        return self.coord[0]

    def colonne(self):
        return self.coord[1]

    ##### CACHE / DECOUVERT #####

    def decouvrir(self):
        if self.cache == True:
            self.cache = False
        else:
            pass

    def est_decouverte(self):
        return not self.cache

    ##### BATIMENTS #####

    def contient_batiment(self):
        return self.bat is not None

    def construire_batiment(self, bat):
        if self.constructible and not self.contient_batiment():
            self.bat = bat

    def detruire_batiment(self):
        if self.contient_batiment():
            self.bat = None

    ##### UNITES #####

    def contient_unite(self):
        return self.unite is not None

    def ajouter_unite(self, new_unit:Unite):
        if not self.contient_unite():
            self.unite = new_unit

    def enlever_unite(self):
        if self.contient_unite():
            self.unite = None

    ##### AFFICHAGE #####

    def __str__(self):
        txt = "Case de {0} ({1};{2}) :\n".format(self.terrain, self.ligne(), self.colonne())
        ############
        txt += "\t- Bâtiment :\t"
        if self.contient_batiment():
            txt += self.bat.nom + "\n"
        elif self.constructible:
            txt += "Libre de construction\n"
        else:
            txt += "Non constructible\n"
        ############
        txt += "\t- Unité :\t\t"
        if self.contient_unite():
            txt += self.unite.nom + "\n"
        else:
            txt += "Aucune\n"
        ############
        txt += "\t- Découverte :\t"
        if self.est_decouverte():
            txt += "oui\n"
        else:
            txt += "non\n"
        ############
        return txt

    def __repr__(self):
        return self.str()



if __name__ == '__main__':
    case = Case(6, 5, "Prairie")
    print(case)

    case.decouvrir()
    case.construire_batiment(Batiment("Banque"))
    print(case.get_coordonnees())
    print(case)

    case.ajouter_unite(Unite("Guerrier", 0, 0))
    print(case)